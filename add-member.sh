#!/bin/bash

if [ -z "${PRIVATE_TOKEN}" ]; then 
    echo "Error:  Missing PRIVATE_TOKEN environment variable!"
    echo "Set Env Variable PRIVATE_TOKEN=<your GitLab Access Token> and retry"
    exit 1
fi


USERNAME="${1}"
GROUPID="${2}"
ACCESS_LEVEL="${3}"

if [ -z "${ACCESS_LEVEL}" ]; then 
    echo "Error:  Missing USERNAME GROUPID or ACCESS_LEVEL"
    echo "Usage: add-member.sh username 012345 00"
    exit 1
fi

BASE_URL="https://gitlab.com/api/v4"

### Get USERID from USERNAME

ENDPOINT="/users"
QUERY="username=${USERNAME}"
REQUEST_TYPE="GET"
RESPONSE=$(curl -s --request ${REQUEST_TYPE} \
     --header "PRIVATE-TOKEN: ${PRIVATE_TOKEN}" \
     -G -d "${QUERY}" \
     "${BASE_URL}${ENDPOINT}")

USERID=$(echo $RESPONSE | jq -r '.[].id')

if [ -z "${USERID}" ]; then
    echo "Error:  User $USERNAME not found at $BASE_URL"
    exit 1
fi

### Add USERID to GROUPID

REQUEST_TYPE="POST"
ENDPOINT="/groups/${GROUPID}/members" 
POST_DATA="user_id=${USERID}&access_level=${ACCESS_LEVEL}" 

RESPONSE=$(curl -s --request ${REQUEST_TYPE} \
    --header "PRIVATE-TOKEN: ${PRIVATE_TOKEN}" \
    --data "${POST_DATA}" \
    "${BASE_URL}${ENDPOINT}")

MEMBERSHIP=$(echo ${RESPONSE} | jq -r '.membership_state')

if [ -z "${MEMBERSHIP}" ]; then
    echo "Error:  User ${USERNAME} with userid ${USERID} not active in groupid ${GROUPID}"
    exit 1
else
    echo "SUCCESS:  User ${USERNAME} active in groupid ${GROUPID}"
fi
