#!/bin/bash

if [ -z "${PRIVATE_TOKEN}" ]; then 
    echo "Error:  Missing PRIVATE_TOKEN environment variable!"
    echo "Set Env Variable PRIVATE_TOKEN=<your GitLab Access Token> and retry"
    exit 1
fi

REQUEST_TYPE="GET"
BASE_URL="https://gitlab.com/api/v4"
ENDPOINT="/groups/54797436/subgroups" # unilogik-gitops-workshop/classgroup1
POST_JSON_DATA='{ }'

if [ "${REQUEST_TYPE}" == "GET" ]; then

    RESPONSE=$(curl -s --request ${REQUEST_TYPE} \
         --header "PRIVATE-TOKEN: ${PRIVATE_TOKEN}" \
         "${BASE_URL}${ENDPOINT}")

elif [ "$REQUEST_TYPE}" == "POST" ]; then
    RESPONSE=$(curl -s --request ${REQUEST_TYPE} \
         --header "PRIVATE-TOKEN: ${PRIVATE_TOKEN}" \
         --header "Content-Type: application/json" \
         --data "${POST_JSON_DATA}" \
         "${BASE_URL}${ENDPOINT}")
fi

#### Do something with RESPONSE (ie: turn it into VALUES via jq)

VALUES=$(echo $RESPONSE | jq -r '.[].path')
echo "
gitops:
  manifest_projects:"

for args in $VALUES
do
echo "
  - id: unilogik-gitops-workshop/classgroup1/${args}/world-greetings-env-1
    default_namespace: default
    paths:
    - glob: '/manifests/**/*.yaml'
    reconcile_timeout: 3600s # 1 hour by default
    dry_run_strategy: none # 'none' by default
    prune: true # enabled by default
    prune_timeout: 360s # 1 hour by default
    prune_propagation_policy: foreground # 'foreground' by default
    inventory_policy: must_match # 'must_match' by default
"
done
